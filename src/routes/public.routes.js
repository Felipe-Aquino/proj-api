import { Router } from 'express';

import AuthController from '../app/controllers/AuthController';
import UserController from '../app/controllers/UserController';
import FileController from '../app/controllers/FileController';

const publicRouter = Router();

// Authenticate token
publicRouter.post('/auth', AuthController.store);

// Save new user
publicRouter.post('/register', UserController.store);

publicRouter.get('/class/:id', FileController.updateClassification);

export default publicRouter;
