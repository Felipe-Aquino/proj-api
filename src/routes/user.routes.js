import { Router } from 'express';

// import AuthController from '../app/controllers/AuthController';
import UserController from '../app/controllers/UserController';
import FileController from '../app/controllers/FileController';
import NotificationController from '../app/controllers/NotificationController';

const userRouter = Router();

userRouter.get('/', UserController.index);
userRouter.get('/count', UserController.count);
userRouter.post('/', UserController.store);
userRouter.post('/del', UserController.deleteByEmail);
userRouter.put('/', UserController.update);
userRouter.delete('/:id', UserController.delete);

/* Files */

userRouter.get('/file', FileController.index);
userRouter.get('/file/count', FileController.count);
userRouter.get('/file/search', FileController.search);
userRouter.post('/file', FileController.store);
userRouter.put('/file/:id', FileController.update);
userRouter.delete('/file/:id', FileController.delete);
userRouter.post('/file/upload', FileController.upload);

/* Notifications */

userRouter.get('/notif', NotificationController.index);
userRouter.get('/notif/count', NotificationController.count);
userRouter.get('/notif/search', NotificationController.search);
userRouter.post('/notif', NotificationController.store);
userRouter.put('/notif/:id', NotificationController.update);
userRouter.delete('/notif/:id', NotificationController.delete);

export default userRouter;
