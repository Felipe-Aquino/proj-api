import { Router } from 'express';

import publicRoutes from './public.routes';
import userRoutes from './user.routes';

import authMiddleware from '../app/middlewares/auth';

const routes = new Router();

routes.use('/public', publicRoutes);
routes.use('/user', authMiddleware, userRoutes);

export default routes;
