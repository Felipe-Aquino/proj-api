import 'dotenv/config';

import express from 'express';
import 'express-async-errors';
import http from 'http';
import path from 'path';
import cors from 'cors';
import AppError from './app/errors/AppError';

import routes from './routes';

import Database from './database';


class App {
  constructor() {
    this.app = express();
    this.server = http.Server(this.app);

    this.database = new Database();

    this.middlewares();
    this.routes();
    this.exceptionHandler();
  }


  middlewares() {
    this.app.use(express.json());

    this.app.use((req, res, next) => {
      next();
    });

    this.app.use(cors());
  }

  routes() {
    this.app.use(routes);
  }

  exceptionHandler() {
    this.app.use(async (err, req, res, next) => {
      if (err instanceof AppError) {
        console.log('error', `AppError(${err.statusCode}): ${err.message}`);
        return res.status(err.statusCode).json({
          status: 'error',
          message: err.message,
          errorType: err.errorType,
          success: false,
        });
      }

      console.log('error', `Internal server error: ${err.message}`);

      return res.status(500).json({
        error: 'Internal server error',
        success: false,
      });
    });
  }
}

export default new App().server;
