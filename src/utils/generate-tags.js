const path = require('path');
const { exec } = require('child_process');

function execCmd(cmd) {
  return new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout);
      }
    });
  });
}

export default async function run(filepath, lang) {
  let taggerpath = process.env.TAGGER_PATH_EN;
  let encoding = '';

  if (lang === 'pt') {
    taggerpath = process.env.TAGGER_PATH_PT;
    encoding = '-enc Latin1';
  }
  else if (lang === 'es') {
    taggerpath = process.env.TAGGER_PATH_ES;
    encoding = '-enc Latin1';
  }

  // console.log('path', path.resolve('.') + filepath);

  // long running task
  const pdf2txt = `pdftotext ${encoding} -layout "${path.resolve('.') + filepath}" -`;
  const awk_tr = "awk \'{gsub(/[\"*^%%&(){}\\[\\]#@$,\\/.!?~=+:;]/,\"\")} 1\' | tr -d '\\014' | tr A-Z a-z";
  const command = `${pdf2txt} | ${awk_tr} > ./dumped_1.txt`;

  await execCmd(command);

  const raw = await execCmd(taggerpath + ' dumped_1.txt');
  const lines = raw.split('\n');

  const result = new Map();

  for (const line of lines) {
    let [ name, tag, lit ] = line.split('\t');

    if (tag && (tag[0] === 'A' || tag[0] === 'N')) {
      if (lit === '<unknown>') {
        lit = name;
      }

      const e = result.get(lit);
      if (!e) {
        result.set(lit, { keyword: lit, count: 1 });
      } else {
        e.count += 1;
      }
    }
  }

  // console.log('-----', command, result);
  return Array.from(result.values());
}

