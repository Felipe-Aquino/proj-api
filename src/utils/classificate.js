import fetch from 'node-fetch';
import partition from './partition';

// TODO: Translate before classificate

function fetch_with_timeout(url, timeout = 30000) {
  const config = {
    headers: {
      Accept: 'application/json',
    },
  }; 

  return new Promise(async (resolve, reject) => {
    let is_timed_out = false;

    const t = setTimeout(() => {
      reject({ msg: 'Error fetch_with_timeout timed out.' });
      is_timed_out = true;
    }, timeout);

    try {
      const response = await fetch(url, config);

      if (is_timed_out) {
        return;
      }

      clearTimeout(t);

      if (!response.ok) {
        reject({ msg: 'Error with status ' + response.status });
        return;
      }

      const result = await response.json();

      resolve(result);
    } catch (err) {
      reject({ msg: 'Error thrown from node-fetch', info: err });
    }
  });
}

function get_entity_uri(queryString, queryClass = null, maxHits = 10) {
  const url = 'http://lookup.dbpedia.org/api/search/KeywordSearch?';
  const params = [];

  if (queryClass) params.push('QueryClass=' + queryClass);
  if (maxHits) params.push('MaxHits=' + maxHits);
  if (queryString) params.push('QueryString=' + encodeURIComponent(queryString));

  params.push('format=JSON');
  return url + params.join('&');
  // return `http://lookup.dbpedia.org/api/search/KeywordSearch?QueryClass=${queryClass}&MaxHits=5&QueryString=${encodeURIComponent(queryString)}`;
}

async function call_dbpedia(url) {
  const response = await fetch_with_timeout(url);

  const data = response.docs.map((doc) => {
    const {
      resource,
      label: name,
      type = [],
      category = [],
    } = doc;

    const classes = type.map((t) => {
      return {
        uri: t,
        label: t.replace('http://dbpedia.org/ontology/', ''),
      };
    });

    const categories = category.map((c) => {
      return {
        uri: c,
        label: c.replace('http://dbpedia.org/resource/Category:', ''),
      };
    })

    return {
      classes,
      categories,
      uri: resource,
      name,
      // uriForDisplay: uri,
      // id: uri,
      // repository: 'dbpedia',
      // description
    };
  });

  const categories = [];

  for (const info of data) {
    categories.push(...info.categories);
  }

  return categories;
}

export default async function classificate(keywords) {
  const partitions = partition([...keywords], 5);

  const classifications = [];

  await Promise.all(partitions.map(async (p) => {
    for (let keyword of p) {
      const word = keyword.translation || keyword.word;
      const url = get_entity_uri(word); 

      const data = await call_dbpedia(url);
      keyword.classifications = data;
      classifications.push(...data);
    }
  }));

  // console.log('classifications', classifications);
  return classifications;
}
