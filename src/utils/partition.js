export default function partition(array, n) {
  const result = [];

  while (array.length > 0) {
    result.push(array.splice(0, n));
  }

  return result;
}
