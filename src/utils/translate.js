import token from 'google-translate-token';
import got from 'got';

export default async function translate(text, from, to = 'en') {
  const result = {
    success: true,
  };

  try {
    const tk = await token.get(text);

    const params = [
      'client=gtx',
      `sl=${from}`,
      `tl=${to}`,
      'dt=t',
      `q=${encodeURI(text)}`,
      `tk=${tk.value}`,
    ].join('&');

    const url = 'https://translate.googleapis.com/translate_a/single?' + params;

    const response = await got(url);

    const [ data ] = JSON.parse(response.body);

    result.value = data[0][0];
  } catch (e) {
    console.log('Translation Error of "%s"', text, e);
    result.success = false;
  }

  return result;
}
