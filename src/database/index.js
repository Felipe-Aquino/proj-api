import config from '../config/database';

import bluebird from 'bluebird';
import mongoose from 'mongoose';

export default class Database {
  constructor() {
    this.init();
  }

  async init() {
    this.mongoose = mongoose;

    if (!process.env.DB_URL) {
      this.url = `mongodb://${config.host}:${config.port}/${config.database}`;
    } else {
      this.url = process.env.DB_URL;
    }

    mongoose.Promise = bluebird;

    try {
      await mongoose.connect(this.url, { useNewUrlParser: true });
      console.log(`Succesfully Connected to the Mongodb Database at URL: ${this.url}`);
    } catch (e) {
      console.log(`Error Connecting to the Mongodb Database at URL: ${this.url}.`, e);
    }
  }
}
