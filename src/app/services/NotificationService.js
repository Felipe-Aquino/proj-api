import Notification from '../models/notification.model';

class NotificationService {
  async getNotifications(query, page, limit) {
    const options = { page, limit };

    try {
      const notifications = await Notification.paginate(query, options);
      return notifications;
    } catch (e) {
      throw Error('Error while Paginating Notifications');
    }
  }

  async createNotification(notification) {
    try{
      const newNotification = new Notification(notification);
      const savedNotification = await newNotification.save();

      return savedNotification;
    } catch (e) {
      throw Error('Error while Creating Notification: ' + e.message);
    }
  }

  async createNotifications(notifications) {
    try{
      const savedNotifications = await Notification.create(notifications);

      return savedNotifications;
    } catch (e) {
      throw Error('Error while Creating Notification: ' + e.message);
    }
  }

  async updateNotification(id, notification) {
    try {
      let n = await Notification.updateOne({_id: id }, {$set: notification});
      return n;
    } catch (e) {
      throw Error('Error occured while update the Notification');
    }
    return null;
  }

  async deleteNotification(id) {
    try {
      const deleted = await Notification.remove({ _id: id });
      return deleted;

    } catch (e) {
      throw Error('Error occured while deleting the Notification.');
    }
  }

  async countNotifications(query) {
    try{
      let count = 0;
      await Notification.count(query, (err, c) => count = c);

      return count;

    } catch (e) {
      throw Error('Error while creating user');
    }
  }
}

export default new NotificationService();
