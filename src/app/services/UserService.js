import User from '../models/user.model';

class UserService {
  async getUsers(query, page, limit) {
    const options = { page, limit };

    try {
      const users = await User.paginate(query, options);
      return users;
    } catch (e) {
      throw Error('Error while Paginating Users');
    }
  }

  async findUser(email) {
    const filter = { email };

    try {
      const user = await User.findOne(filter);
      return user;
    } catch (e) {
      throw Error('Error to find user');
    }
  }

  async createUser(user) {
    try{
      const newUser = new User(user);
      const savedUser = await newUser.save();

      return savedUser;

    } catch (e) {
      throw Error('Error while creating user');
    }

  }

  async updateUserById(id, user) {
    try {
      return await User.updateOne({ _id: id }, { $set: user });
    } catch (e) {
      throw Error('An error occured while updating the user.');
    }

    return null;
  }

  async updateUserByEmail(email, user) {
    try {
      return await User.updateOne({ email }, { $set: user });
    } catch (e) {
      throw Error('An error occured while updating the user.');
    }

    return null;
  }

  async deleteUser(id) {
    try {
      const deleted = await User.remove({ _id: id });

      if (deleted.result.n === 0) {
        throw Error('User could not be deleted.');
      }

      return deleted;

    } catch (e) {
      throw Error('Error occured while deleting the user.');
    }
  }

  async deleteUserByEmail(email) {
    try {
      const deleted = await User.deleteOne({ email });
      return deleted;
    } catch (e) {
      throw Error('Error occured while deleting the user.');
    }
  }

  async countUsers() {
    try {
      let count = 0;
      await User.count({}, (err, c) => count = c);

      return count;
    } catch (e) {
      throw Error('Error while counting users');
    }
  }
}

export default new UserService();
