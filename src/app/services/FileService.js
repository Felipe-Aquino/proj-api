import File from '../models/file.model';

class FileService {
  async getFiles(query, page, limit) {
    const options = { page, limit };

    try {
      const files = await File.paginate(query, options);
      return files;
    } catch (e) {
      throw Error('Error while Paginating Files');
    }
  }

  async getFile(id) {
    try {
      const files = await File.findById(id);
      return files;
    } catch (e) {
      throw Error('Error while Paginating Files');
    }
  }

  async createFile(file) {
    try{
      const newFile = new File(file);
      const savedFile = await newFile.save();

      return savedFile;
    } catch (e) {
      throw Error('Error while Creating File: ' + e.message);
    }

    return null;
  }

  async updateFile(file, id) {
    try {
      const f = await File.findOneAndUpdate({ _id: id }, { ...file }, { new: true });
      return f;
    } catch (e) {
      console.log('error file service', e);
      throw Error('Error occured while update the file');
    }

    return null;
  }

  async deleteFile(id) {
    try {
      const deleted = await File.remove({ _id: id });
      return deleted;
    } catch (e) {
      throw Error('Error occured while deleting the File.');
    }

    return null;
  }

  async countFiles(query) {
    try{
      let count = 0;
      await File.count(query, (err, c) => count = c);

      return count;
    } catch (e) {
      throw Error('Error while creating user');
    }
  }
}

export default new FileService();
