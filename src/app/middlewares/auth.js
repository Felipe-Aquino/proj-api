import jwt from 'jsonwebtoken';
import { promisify } from 'util';

import AppError from '../errors/AppError';
import HttpStatus from '../../utils/http-status';

import authConfig from '../../config/auth';

export default async (req, res, next) => {
  let token = req.headers.authorization;

  if (!token) {
    token = req.query.token;
  }

  if (!token) {
    throw new AppError(
      'Failed to authenticate.',
      HttpStatus.UNAUTHORIZED,
      'token_not_sent'
    );
  }

  try {
    const decoded = await promisify(jwt.verify)(token, authConfig.secret);

    req.decoded = decoded;

    return next();
  } catch (err) {
    throw new AppError(
      'Failed to authenticate.',
      HttpStatus.UNAUTHORIZED,
      'invalid_token'
    );
  }
};
