class AppError {
  constructor(message, statusCode = 400, errorType = 'default_error') {
    this.message = message;
    this.statusCode = statusCode;
    this.errorType = errorType;
  }
}

export default AppError;
