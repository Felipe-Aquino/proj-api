import User from '../models/user.model';
import UserService from '../services/UserService';
import NotificationService from '../services/NotificationService';

import HttpStatus from '../../utils/http-status';

class NotificationController {
  async index(req, res) {
    const admin = req.decoded.admin;
    const {
      page = 1,
      limit = 10,
    } = req.query;

    const receiver = admin ? null : req.decoded.id;

    try {
      if (!admin) {
        const notifications =
          await NotificationService.getNotifications({ receiver }, page, limit);

        const info = notifications.docs.map((notif) => {
          return {
            id: notif._id,
            title: notif.title,
            message: notif.message,
            read: notif.read,
            relevance: notif.relevance,
            creation: notif.creation
          };
        });

        return res.status(HttpStatus.OK).json({ data: info });
      }


      const notifications =
        await NotificationService.getNotifications({ receiver }, page, limit);

      const senders = notifications.docs.map((doc) => doc.sender);
      const info = [];
      
      const users = User.paginate({_id: { $in: senders }}, { page, limit })

      for (const notif of notifications.docs) {
        const user = users.docs.find(u => u._id.equals(notif.sender));

        if (user) {
          info.push({
            id: notif._id,
            title: notif.title,
            message: notif.message,
            read: notif.read,
            relevance: notif.relevance,
            creation: notif.creation,
            sender: {
              name: user.name,
              email: user.email,
            },
          });
        }
      }

      return res.status(HttpStatus.OK).json({ data: info });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
    }
  }

  async search(req, res) {
    const admin = req.decoded.admin;
    const {
      page = 1,
      limit = 100,
    } = req.query;

    const receiver = admin ? null : req.decoded.id;
    const title = req.query.title || '';

    try {
      const notifications =
        await NotificationService.getNotifications({ receiver }, page, limit);

      let info = [];

      const re = new RegExp(title, 'i');

      for (const doc of notifications.docs) {
        if (doc.title.search(re)) {
          info.push({
            id: doc._id,
            title: doc.title,
            message: doc.message,
            read: doc.read,
            relevance: doc.relevance,
            creation: doc.creation,
          });
        }
      }

      return res.status(HttpStatus.OK).json({ data: info });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
    }
  }

  async store(req, res) {
    const { id: sender, admin } = req.decoded;

    const {
      emails,
      title,
      message,
      relevance,
    } = req.body;

    const notification = {
      title: title,
      message: message,
      relevance: relevance,
      sender,
      receiver: null
    };

    try {
      if (!admin) {
        await NotificationService.createNotification(notification);

        return res.status(HttpStatus.CREATED).json({
          message: 'Notification succesfully created.',
        });
      }

      const users = UserService.getUsers({ email: { $in: emails } }, 1, 10);

      const notifications = user.docs.map((user) => {
        return {
          ...notification,
          receiver: user._id
        };
      });

      // console.log(notifications);
      await NotificationService.createNotifications(notifications);

      return res.status(HttpStatus.CREATED).json({
        message: 'Notification succesfully created.',
      });
    } catch (e) {
      console.log('err', e);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Creation of the notification was unsuccesfull.',
      });
    }
  }

  async update(req, res) {
    const id = req.params.id;

    if (!id) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Id must be present.',
      });
    }

    const notification = {
      read: req.body.read || false,
    };

    try {
      const updated = await NotificationService.updateNotification(id, notification);

      return res.status(HttpStatus.OK).json({
        message: 'Notification succesfully updated.',
      });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
    }
  }

  async delete(req, res) {
    const id = req.params.id;

    try {
      await NotificationService.deleteNotification(id);

      return res.status(HttpStatus.NO_CONTENT).json({
        message: 'Notification succesfully deleted.',
      });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
    }
  }

  async count(req, res) {
    const admin = req.decoded.admin;
    const receiver = admin ? null : req.decoded.id;

    try {
      let count = 0;

      if (admin) {
        count = await NotificationService.countNotifications({});
      } else {
        count = await NotificationService.countNotifications({ receiver });
      }

      return res.status(HttpStatus.OK).json({ data: count });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({ message: e.message });
    }
  }
}

export default new NotificationController();
