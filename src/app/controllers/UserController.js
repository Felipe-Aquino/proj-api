import UserService from '../services/UserService';
import HttpStatus from '../../utils/http-status';

import bcrypt from 'bcrypt';

class UserController {
  async index(req, res) {
    const {
      page = 1,
      limit = 10,
    } = req.query;

    const { id, admin } = req.decoded;

    try {
      let users = null;
      if (admin) {
        users = await UserService.getUsers({}, page, limit);
      } else {
        users = await UserService.getUsers({_id: id}, page, limit);
      }

      const users_info = users.docs.map((doc) => {
        return {
          id: doc.id,
          name: doc.name,
          email: doc.email,
          admin: doc.admin,
        };
      });

      // the object of the user the user which requested
      const idx = users_info.find((doc) => doc.id === id);

      if (idx >= 0) {
        let x = users_info.splice(idx, 1);
        users_info.unshift(x[0]);
      }

      return res.status(HttpStatus.OK).json({
        data: users_info,
      });

    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async show(req, res) {
    const {
      page = 1,
      limit = 10,
    } = req.query;

    const { id, admin } = req.decoded;

    try {
      let users = await UserService.getUsers({_id: id}, page, limit);

      const users_info = users.docs.map((doc) => {
        return {
          name: doc.name,
          email: doc.email,
          admin: doc.admin,
        };
      });

      return res.status(HttpStatus.OK).json({
        data: users_info[0],
      });

    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async store(req, res) {
    const { name, email, admin, password } = req.body;

    const saltRounds = 10;

    try {
      const salt = bcrypt.genSaltSync(saltRounds);
      const hash = bcrypt.hashSync(req.body.password, salt);

      const exists = await UserService.getUsers({ email }, 1, 10);

      if (exists.docs.length) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          message: 'Creation of user was unsuccesfull.',
        });
      }

      const user = {
        name,
        email,
        admin,
        password: hash,
      };

      await UserService.createUser(user);

      return res.status(HttpStatus.CREATED).json({
        message: 'User succesfully created.',
      });

    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Creation of user was unsuccesfull.',
      });
    }
  }

  async update(req, res) {
    const { name = '', email = '', admin = false } = req.body;

    const user = {
      name,
      admin,
    };

    try {
      const updated = await UserService.updateUserByEmail(email, user);

      return res.status(HttpStatus.OK).json({
        data: updated,
      });
    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  /* Deletes the current user */
  async delete(req, res) {
    const id = req.decoded.id;

    try {
      await UserService.deleteUser(id);

      return res.status(HttpStatus.NO_CONTENT).json({
        message: 'User succesfully deleted.',
      });
    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  /* Deletes an user by email */
  async deleteByEmail(req, res) {
    const { email } = req.body;

    try {
      await UserService.deleteUserByEmail(email);

      return res.status(HttpStatus.NO_CONTENT).json({
        message: 'User succesfully deleted.',
      });
    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async count(req, res) {
    const admin = req.decoded.admin;

    try {
      let count = 1;

      if (admin) {
        count = await UserService.countUsers();
      }

      return res.status(HttpStatus.OK).json({
        data: count,
      });

    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }
}

export default new UserController();
