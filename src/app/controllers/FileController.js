import fs from 'fs';
import generateTags from '../../utils/generate-tags';

import multiparty from 'multiparty';

import fetch from 'node-fetch';

import translate from '../../utils/translate';
import classificate from '../../utils/classificate';

import FileService from '../services/FileService';
import HttpStatus from '../../utils/http-status';


class FileController {
  async index(req, res) {
    const {
      page = 1,
      limit = 10,
    } = req.query;

    const user_id = req.decoded.id;

    try {
      const files = await FileService.getFiles({ user_id }, page, limit);

      const files_info = files.docs.map((file) => {
        return {
          _id: file._id,
          name: file.name,
          language: file.language,
          classification: file.classification,
          keywords: file.keywords,
          given_keywords: file.given_keywords,
          public_: file.public,
          creation: file.creation,
        };
      });

      return res.status(HttpStatus.OK).json({
        data: files_info,
      });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async search(req, res) {
    const {
      page = 1,
      limit = 100,
      value,
      type,
    } = req.query;

    try {
      const files = await FileService.getFiles({}, page, limit);

      let files_info = files.docs.map((file) => {
        return {
          _id: file._id,
          name: file.name,
          classification: file.classification,
          keywords: file.keywords,
          given_keywords: file.given_keywords,
          public_: file.public,
          creation: file.creation
        };
      });

      files_info = searchInFiles(files_info, value, type);

      return res.status(HttpStatus.OK).json({
        data: files_info,
      });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async store(req, res) {
    const user_id = req.decoded.id;

    const {
      name,
      lang,
      filename,
      given_keywords,
    } = req.body;

    const path = '/uploads/' + filename;

    const file = {
      user_id,
      name,
      path,
      language: lang,
      keywords: [],
      given_keywords: [],
    };

    if (given_keywords) {
      file.given_keywords = given_keywords.map((word) => {
        return {
          word,
          translation: null,
          classifications: null,
        };
      });
    }

    try {
      let result = await generateTags(path, lang);
      // console.log(result);

      // console.log('createFile, tags', result);
      result.sort((fst, snd) => snd.count - fst.count);

      // TODO: Verificar valor para o máximo de tags total
      const total = Math.min(30, result.length);

      for (let i = 0; i < total; i++) {
        const { keyword, count } = result[i];

        if (!keyword || keyword.length < 3) {
          continue;  
        }

        file.keywords.push({
          word: keyword,
          count,
          translation: null,
          classifications: null,
        });
      }

      // console.log('createFile, 164: file', file);
      const createdFile = await FileService.createFile(file);

      make_classification(createdFile);

      return res.status(HttpStatus.CREATED).json({
        message: 'File succesfully created.',
      });
    } catch (e) {
      console.log('file error', e);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Creation of file was unsuccesfull.',
      });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    if (!id) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Id must be present.',
      });
    }

    const {
      public_ = false,
      keywords,
      given_keywords,
    } = req.body;

    const file = {
      public: public_,
      keywords,
      given_keywords,
    };

    try {
      const updated = await FileService.updateFile(file, id);

      return res.status(HttpStatus.OK).json({
        message: 'File succesfully updated.',
      });
    } catch (e) {
      console.log('error file update', e, id);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    try {
      await FileService.deleteFile(id);

      return res.status(HttpStatus.NO_CONTENT).json({
        message: 'File succesfully deleted.',
      });
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async count(req, res) {
    const { id: user_id, admin } = req.decoded;

    try {
      let result = 0;

      if (admin) {
        result = await FileService.countFiles({});
      } else {
        result = await FileService.countFiles({ user_id });
      }

      return res.status(HttpStatus.OK).json({
        data: result,
      });

    } catch(e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async upload(req, res) {
    try {
      const form = new multiparty.Form();

      form.on('part', (part) => part.resume());

      form.on('file', (name, file) => {
        const tmp_path = file.path;
        const target_path = './uploads/' + file.originalFilename;

        fs.renameSync(tmp_path, target_path, function(err) {
          if (err) {
            console.error(err.stack);
          }
        });

        const data = {
          name: file.originalFilename,
          size: file.size,
        };

        return res.status(HttpStatus.OK).json({ data });
      });

      form.parse(req);

    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }

  async updateClassification(req, res) {
    const { id } = req.params;

    try {
      const updated = await FileService.updateFile({ processing: true }, id);

      make_classification(updated);

      return res.status(HttpStatus.OK).json({ data });
    } catch (e) {
      console.log('updateClassification error', e);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: e.message,
      });
    }
  }
}


function searchInFiles(files, value, stype) {
  if (!value) {
    return [];
  }

  const re = new RegExp(value, 'i');

  const nameMatch = (name) => {
    return name.search(re) != -1;
  }

  switch (stype) {
  case 'N':
    return files.filter((file) => file.public_ && nameMatch(file.name));
  case 'K':
    return files.filter((file) => {
      if (file.public_) {
        let idx = file.keywords.findIndex((k) => nameMatch(k.word));

        if (idx > -1) {
          return true;
        }

        idx = file.given_keywords.findIndex((k) => nameMatch(k.word));

        return idx > -1;
      }

      return false;
    });
  case 'C':
    //@Refactor: Redo the search by classification
    // result = files.filter((_value, index, obj) => _value.public_ && nameMatch(_value.classification, value));
    break;
  }

  return [];
}

async function make_classification(file) {
  const { _id, keywords, given_keywords } = file;  

  try {
    await classificate(keywords);
    // keywords.classifications = classifications;

    await classificate(given_keywords);
    // given_keywords.classifications = classifications;

  } catch (e) {
    console.log('Error to classificate file', _id, e);
    return;
  }

  try {
    file.processing = false;

    // console.log('Saving', keywords, given_keywords, file, _id);
    await FileService.updateFile({
      keywords,
      given_keywords,
    }, _id);
  } catch (e) {
    // TODO: Maybe find a better solution
    console.log('Error to update file after classification', _id);
  }
}

export default new FileController();
