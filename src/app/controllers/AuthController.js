import UserService from '../services/UserService';
import HttpStatus from '../../utils/http-status';

import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import config from '../../config/auth';

class AuthController {
  async store(req, res) {
    const { email, password } = req.body;

    const user = await UserService.findUser(email);

    if (!user) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        success: false,
        message: 'Authentication failed.'
      });
    }

    const success = await bcrypt.compare(password, user.password);

    if (!success) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        success: false,
        message: 'Authentication failed.'
      });
    }

    // if user is found and password is right
    // create a token
    const token = jwt.sign(
      { admin: user.admin, id: user._id },
      config.secret,
      { expiresIn: config.expiresIn }
    );

    return res.status(HttpStatus.OK).json({
      success: true,
      message: 'Enjoy your token!',
      admin: user.admin,
      token
    });
  }
}

export default new AuthController();

