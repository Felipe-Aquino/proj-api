import { Schema, model } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const URI = {
  label: String,
  uri: String
};

const FileSchema = new Schema({
  user_id: Schema.ObjectId,
  name: String,
  path: String,
  language: String,
  classification: String,
  keywords: [{
    word: String,
    count: Number,
    translation: String,
    classifications: [URI]
  }],
  given_keywords: [{
    word: String,
    translation: String,
    classifications: [URI]
  }],
  public: {
    type: Boolean,
    default: true
  },
  creation: {
    type: Date,
    default: Date.now
  },
  processing: {
    type: Boolean,
    default: true
  }, 
});

FileSchema.plugin(mongoosePaginate);
const File = model('File', FileSchema);

export default File;
