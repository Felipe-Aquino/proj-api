import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const NotificationSchema = new mongoose.Schema({
  sender: mongoose.Schema.ObjectId,
  receiver: mongoose.Schema.ObjectId,
  title: String,
  message: String,
  read: {
    type: Boolean,
    default: false
  },
  relevance: {
    type: Number,
    default: 0,
    min: 0,
    max: 2
  },
  creation: {
    type: Date,
    default: Date.now
  }
});

NotificationSchema.plugin(mongoosePaginate);
const Notification = mongoose.model('Notification', NotificationSchema);

export default Notification;

