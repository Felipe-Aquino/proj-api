import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  admin: Boolean
});

UserSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', UserSchema);

export default User;
