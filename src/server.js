import app from './app';

app.listen(process.env.PORT || 3333);

console.log(
  'info',
  `server listening on port ${process.env.PORT ? process.env.PORT : '3333'}`
);
